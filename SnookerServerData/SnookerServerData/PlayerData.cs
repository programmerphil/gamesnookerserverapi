﻿

namespace SnookerGameAPI.Models
{
    public class PlayerData
    {
        public long Id { get; set; }
        public long GameId { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }

        public GameData Game { get; set; }

        public PlayerData()
        {
            
        }

        public PlayerData(string name, long gameId) : this(name, gameId, 0)
        { 
            
        }

        public PlayerData(string name, long gameId, int score)
        {
            GameId = gameId;
            Name = name;
            Score = score;
        }
    }
}