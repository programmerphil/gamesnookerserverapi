﻿namespace SnookerGameAPI.Models
{
    public class GameUpdateData
    {
        public int Id { get; set; }
        public int? Player1Score { get; set; }
        public int? Player2Score { get; set; }
        public int? CurrentFrame { get; set; }
    }
}