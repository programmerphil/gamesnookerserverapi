﻿namespace SnookerGameAPI.Models
{
    public class GameStartData
    {
        public long Id { get; set; }
        public string Player1Name { get; set; }
        public string Player2Name { get; set; }
        public int MaxFrames { get; set; }
    }
}