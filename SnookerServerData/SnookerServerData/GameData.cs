﻿using System.Collections.Generic;

namespace SnookerGameAPI.Models
{
    public class GameData
    {
        public long Id { get; set; }
        public List<PlayerData> Players { get; set; }
        public int MaxFrames { get; set; }
        public int CurrentFrame { get; set; }

        public GameData(GameStartData gameStartData) : this(gameStartData.MaxFrames)
        {
            
        }

        public GameData()
        {
        }

        public GameData(int maxFrames)
        {
            MaxFrames = maxFrames;
            CurrentFrame = 0;
        }

        public void UpdatePlayerScore(int index, int score)
        {
            Players[index].Score = score;
        }
    }
}