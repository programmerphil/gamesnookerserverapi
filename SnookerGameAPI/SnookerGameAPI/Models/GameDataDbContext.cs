﻿using Microsoft.EntityFrameworkCore;
using SnookerServerDataV2;

namespace SnookerGameAPI.Models
{
    public class GameDataDbContext : DbContext
    {
        public GameDataDbContext(DbContextOptions<GameDataDbContext> options)
            : base(options)
        {
        }

        public DbSet<GameData> Games { get; set; }
        public DbSet<PlayerData> Players { get; set; }
    }
}