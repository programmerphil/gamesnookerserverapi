using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SnookerGameAPI.Models;
using SnookerServerDataV2;

namespace SnookerGameAPI.Controllers
{
    [Route("api/[controller]")]
    public class GameDataController : Controller
    {
        private readonly GameDataDbContext _context;

        public GameDataController(GameDataDbContext context)
        {
             _context = context;
            if (_context.Games.Count() <= 0)
            {
                _context.Games.Add(new GameData(3));
                _context.Players.Add(new PlayerData("Tihi", 1));
                _context.Players.Add(new PlayerData("Toho", 1));
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<GameData> GetAll()
        {
            return _context.Games.Include(game => game.Players);
        }

        [HttpGet("{id}", Name = "GetGame")]
        public IActionResult GetById(long id)
        {
            var item = _context.Games.Include(game => game.Players).FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] GameStartData gameStartData)
        {
            if (gameStartData == null)
            {
                return BadRequest();
            }

            GameData game = new GameData(gameStartData);

            _context.Games.Add(game);
            _context.SaveChanges();
            _context.Players.Add(new PlayerData(gameStartData.Player1Name, game.Id));
            _context.Players.Add(new PlayerData(gameStartData.Player2Name, game.Id));
            _context.SaveChanges();

            return CreatedAtRoute("GetGame", new { id = game.Id }, game);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] GameUpdateData item)
        {
            var game = _context.Games.Include(g => g.Players).FirstOrDefault(t => t.Id == id);
            if (game == null)
            {
                return NotFound();
            }

            item.Update(game); 

            _context.Games.Update(game);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var todo = _context.Games.First(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();  
            }

            _context.Games.Remove(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}