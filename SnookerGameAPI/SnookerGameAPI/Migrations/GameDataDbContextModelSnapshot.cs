﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SnookerGameAPI.Models;

namespace SnookerGameAPI.Migrations
{
    [DbContext(typeof(GameDataDbContext))]
    partial class GameDataDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("SnookerServerDataV2.GameData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CurrentFrame");

                    b.Property<int>("MaxFrames");

                    b.Property<int>("PlayerTurn");

                    b.HasKey("Id");

                    b.ToTable("Games");
                });

            modelBuilder.Entity("SnookerServerDataV2.PlayerData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("FrameScore");

                    b.Property<long>("GameId");

                    b.Property<string>("Name");

                    b.Property<int>("Score");

                    b.HasKey("Id");

                    b.HasIndex("GameId");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("SnookerServerDataV2.PlayerData", b =>
                {
                    b.HasOne("SnookerServerDataV2.GameData", "Game")
                        .WithMany("Players")
                        .HasForeignKey("GameId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
